package oshima_ryokei.dao;


import static oshima_ryokei.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oshima_ryokei.beans.Departments;
import oshima_ryokei.beans.User;
import oshima_ryokei.exception.SQLRuntimeException;



public class UserDao {

	public void insert(Connection connection, User user) {


		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users( ");
			sql.append(" login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", is_stopped");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
			ps.setInt(6, user.getIsStopped());
			ps.executeUpdate();



		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);


		}
	}

	public User getUser(Connection connection, String loginId, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {

				return null;
			} else if (userList.isEmpty() != true) {

				return userList.get(0);
			}
			return null;

			}catch(SQLException e){
				throw new SQLRuntimeException(e);
			}finally{
				close(ps);

		}

	}
	public List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				int isStopped = rs.getInt("is_stopped");


				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setIsStopped(isStopped);



				ret.add(user);


			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public User getUser(Connection connection, String loginId) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, loginId);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	public List<User> getBranches(Connection connection) throws SQLException {

		PreparedStatement bps = null;
		try {
			String bsql = "SELECT * FROM branches";

			bps = connection.prepareStatement(bsql);

			ResultSet brs = bps.executeQuery();

			List<User> branches =  toEditBranches(brs);

			return branches;

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(bps);
		}

	}



	private List<User> toEditBranches(ResultSet brs) throws SQLException {
		List<User> branches = new ArrayList<User>();
		try {
			while(brs.next()){

				int branchId = brs.getInt("id");
				String branchName = brs.getString("name");

				User editBranches = new User();
				editBranches.setBranchId(branchId);
				editBranches.setName(branchName);

			}return branches;

		}finally {
			close(brs);

		}

	}

	public List<Departments> getDepartments(Connection connection) throws SQLException {

		PreparedStatement dps = null;
		try {
			String dsql = "SELECT * FROM departments";

			dps = connection.prepareStatement(dsql);

			ResultSet drs = dps.executeQuery();

			List<Departments> departments =  toEditDepartments(drs);

			return departments;

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(dps);
		}

	}

	private List<Departments> toEditDepartments(ResultSet drs) throws SQLException {
		List<Departments> departments = new ArrayList<Departments>();
		try {
			while(drs.next()){

				int departmentId = drs.getInt("id");
				String departmentName = drs.getString("name");

				Departments editDepartments = new Departments();
				editDepartments.setId(departmentId);
				editDepartments.setName(departmentName);

				departments.add(editDepartments);
			}return departments;

		}finally {
			close(drs);
		}


	}
	public List<User> checkUser(Connection connection, String loginId) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement checkPs = null;
		StringBuilder checkSql = new StringBuilder();
		checkSql.append("SELECT * FROM users where login_id = ?");
		checkPs = connection.prepareStatement(checkSql.toString());

		checkPs.setString(1, loginId);
		ResultSet checkRs = checkPs.executeQuery();

		List<User> checkUserList = toCheckUser(checkRs);
		return checkUserList;

	}


	public List<User> toCheckUser(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				int isStopped = rs.getInt("is_stopped");


				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setIsStopped(isStopped);

				ret.add(user);

			}
			return ret;
		} finally {
			close(rs);
		}

	}


}



