package oshima_ryokei.service;


import static oshima_ryokei.utils.CloseableUtil.*;
import static oshima_ryokei.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import oshima_ryokei.beans.Branches;
import oshima_ryokei.beans.Departments;
import oshima_ryokei.beans.User;
import oshima_ryokei.dao.EditUserDao;
import oshima_ryokei.dao.UserDao;
import oshima_ryokei.utils.CipherUtil;



public class UserService {

	public void register(User user){

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		}catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);

		}
	}
	public User getUser(String loginId) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getUser(connection, loginId);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public List<Branches> toEditBranches() {

		Connection connection = null;
		try {
			connection = getConnection();
			EditUserDao editUserDao = new EditUserDao();
			List<Branches> branches = editUserDao.getBranches(connection);

			commit(connection);

			return branches;

    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}

	public List<Departments> toEditDepartments() {

		Connection connection = null;
		try {
			connection = getConnection();
			EditUserDao editUserDao = new EditUserDao();
			List<Departments> departments = editUserDao.getDepartments(connection);

			commit(connection);

			return departments;

    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
	public List<User> checkUser(String loginId) {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao checkUserDao = new UserDao();
			List<User> checkUser = checkUserDao.checkUser(connection, loginId);


		commit(connection);
		return checkUser;

	}catch (RuntimeException e) {
		rollback(connection);
		throw e;

	} catch (SQLException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	} finally {
		close(connection);

	}

	return null;
	}


}

