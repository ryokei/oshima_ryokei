<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規ユーザー登録</title>
	<script type="text/javascript">
		function checkCreateUser(){
				if(window.confirm('このユーザーを登録しますか？')){
					return true;
				} else{
					window.alert('キャンセルされました');
					return false;
				}
			}
	</script>
</head>
<body>
<div class="main-contents">
<h2>■新規ユーザー登録</h2>
<div class="menu">
<a href="./">ホーム</a>
</div>
<c:if test="${not empty errorMessages}">
<div class="errorMessages">
	<ul>
	<c:forEach items="${errorMessages}" var="messages">
	<li><c:out value="${messages}" />
	</c:forEach>
	</ul>
	</div>
	<c:remove var="errorMessages" scope="session" />
</c:if>
	<h4>◆ユーザー情報入力フォーム</h4>
<form action="createNew" method="post" onSubmit="return checkCreateUser()"><br />
	<label for="name">名前(10文字以内)</label><br />
		<input name="name" id="name" value="${createdUser.name}"/><br />
	<label for="login_id">ログインID(半角英数字6文字以上20文字以内)</label><br />
		<input name="login_id" id="login_id" value="${createdUser.loginId}"/><br />
	<label for="password">パスワード(半角英数字・記号6文字以上20文字以内)</label><br />
		<input name="password" type="password" id="password" value="${createdUser.password}"/><br />
	<label for="password">パスワード確認用</label><br /><input name="passwordCheck" type="password" id="password" /><br />
	支店名<br />
		<select name="branch_id">
			<c:forEach items="${ branches }" var="branch">
					<option value="${branch.id}" <c:if test="${branch.id == createdUser.branchId}" > selected </c:if> >${branch.name}</option>
			</c:forEach>
		</select>
	<br />
	部署・役職<br />
		<select name="department_id">
			<c:forEach items="${ departments }" var="department">
					<option value="${department.id}" <c:if test="${department.id == createdUser.departmentId}" > selected </c:if> >${department.name}</option>
			</c:forEach>
		</select>
		<br />

	<input type="submit" value="登録" /><br />

	</form>
	<div class="copyright">Copyright(c)RyokeiOshima</div>
</div>



</body>
</html>